import React from "react";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      showH1: true,
      showDiv: true,
      showH2: true,
      showH3: true,
      showB: true,
    };
  }

  hideButton = () => {
    this.setState({ show: false });
  };

  showButton = () => {
    this.setState({ show: true });
  };

  hideH1 = () => {
    this.setState({ showH1: false });
  };

  showH1 = () => {
    this.setState({ showH1: true });
  };

  hideDiv = () => {
    this.setState({ showDiv: false });
  };

  showDiv = () => {
    this.setState({ showDiv: true });
  };

  hideH2 = () => {
    this.setState({ showH2: false });
  };

  showH2 = () => {
    this.setState({ showH2: true });
  };

  hideShow = () => {
    this.setState({ showH3: !this.state.showH3 });
  };

  hideShowB = () => {
    this.setState({ showB: !this.state.showB });
  };

  render() {
    return (
      <div className="App-header">
        <div className="div-flex">
          <button onClick={this.hideButton}>Esconda Div!</button>
          <br></br>
          <button onClick={this.showButton}>Mostrar Div!</button>
          {this.state.show && <p>Olá React!</p>}
        </div>

        <div className="div-flex">
          <button onClick={this.hideH1}>Esconda H1!</button>
          <br></br>
          <button onClick={this.showH1}>Mostrar h1!</button>
          {this.state.showH1 && <h1>Olá, H1</h1>}
        </div>

        <div className="div-flex">
          <button onClick={this.hideDiv}>Esconda Div!</button>
          <br></br>
          <button onClick={this.showDiv}>Mostrar Div!</button>
          <br></br>
          {this.state.showDiv && <div>Olá, div</div>}
        </div>

        <div className="div-flex">
          <button onClick={this.hideH2}>Esconda H2!</button>
          <br></br>
          <button onClick={this.showH2}>Mostrar h2!</button>
          {this.state.showH2 && <h2>Olá, H2</h2>}
        </div>

        <div className="div-flex">
          <button onClick={this.hideShow}>Mostra/Esconde H3</button>
          <br></br>
          {this.state.showH3 && <h2>Olá, H3</h2>}
        </div>

        <div className="div-flex">
          <button onClick={this.hideShowB}>Mostra/Esconde B</button>
          <br></br>
          {this.state.showB && <b>Olá, B !</b>}
        </div>
      </div>
    );
  }
}

export default App;
